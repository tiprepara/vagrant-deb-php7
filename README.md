Vagrant-deb-php7
===========

** NÃO RECOMENDÁVEL EM SISTEMAS OPERACIONAIS WINDOWS **

Servidor LAMP (Linux(Debian/64), Apache, MySQL, PHP7)

### Configuracão do Vagrant para criar uma máquina virtual (Debian Server 64 Bits) para desenvolvimento em PHP.


### Packages:

- PHP 7.1
- MySQL 5.5
- Git
- PhpMyAdmin 
- Composer
- cURL
- Vim
- Redis
- Plugin vagrant-vbguest (para o Debian)


**(Consultar o arquivo provision.sh, caso hajam dúvidas)**


Softwares não inclusos: 
==============

- Virtualbox - https://www.virtualbox.org/
- Vagrant - http://www.vagrantup.com/
- Git - http://git-scm.com (E interessante para voce criar seus proprios projetos)

Voce precisará de:
===============

- Acesso Internet

Passos:
===========

0BS -> Git

* Se houver dúvidas sobre o uso e configuração do Git, ver o arquivo GITCOMANDS.md *


1º -> Git Instalado

* Clone esse repositório para sua máquina:

- git clone https://tiprepara@bitbucket.org/tiprepara/vagrant-deb-php7.git

* Rode o comando:

- vagrant up 


2º -> Sem o Git instalado, acesse https://tiprepara@bitbucket.org/tiprepara/vagrant-deb-php7.git e faça o download do arquivo em .zip e descompacte-o.

* Rode o comando:

- vagrant up


3° -> O Debian Server 64 será baixado e configurado na maquina virtual no VirtualBox através do provisionador BASH: 'provision.sh'.

Depois de pronto, o servidor web Apache estará disponível no endereço http://localhost:8083 ou através do endereço local: http://192.168.33.10. O PHPMyAdmin estará em http://localhost:8083/phpmyadmin ou assim: 192.168.33.10/phpmyadmin. E verifique o php através de http://localhost:8083/phpinfo.php ou http://192.168.33.10/phpinfo.php. 


*Para acessar o phpmyadmin utilize:*

- Login: root
- Senha: vagrant


** FACA ISTO CASA HAJA ALGUM CONFLITO EM SUA MAQUINA E NÃO ACESSE O PHPINFO.PHP DIRETAMENTE**

- Ja loagado na VM, crie um arquivo no diretorio html: phpinfo.php:

*<?php phpinfo(); ?>*

- Salve-o e agora sim deverá funcionar: http://localhost:8083/phpinfo.php


O diretório www sera o responsavel pelo compartilhamento entre a máquina fisica e a virtual. E crie seus projetos dentro da
pasta html/.

** Caso haja necessidade, seguir estes dois passos:

- 1° vagrant plugin install vagrant-vbguest
- 2° vagrant plugin install vagrant-winnfsd => Isso se seu SO for Windows.
- 3° Logado na VM: 

  ** $ sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions **

- 4° Não esquecer de executar: vagrant reload

*- Porém, acreditamos que com o plugin instalado não serão necessários esses passos acima.


** Bom desenvolvimento e diversão! **





