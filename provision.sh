#!/bin/bash

echo "---- Iniciando instalacao do ambiente de Desenvolvimento PHP ---"

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update

echo "--- Instalando plugin vagrant guest para Debian ---"
vagrant plugin install vagrant-vbguest 

echo "--- Instalando plugin vagrant guest para Windows ---"
vagrant plugin install vagrant-winnfsd

echo "--- Definindo Senha padrao para o MySQL e suas ferramentas ---"

DEFAULTPASS="vagrant"
sudo debconf-set-selections <<EOF
mysql-server	mysql-server/root_password password $DEFAULTPASS
mysql-server	mysql-server/root_password_again password $DEFAULTPASS
dbconfig-common	dbconfig-common/mysql/app-pass password $DEFAULTPASS
dbconfig-common	dbconfig-common/mysql/admin-pass password $DEFAULTPASS
dbconfig-common	dbconfig-common/password-confirm password $DEFAULTPASS
dbconfig-common	dbconfig-common/app-password-confirm password $DEFAULTPASS
phpmyadmin		phpmyadmin/reconfigure-webserver multiselect apache2
phpmyadmin		phpmyadmin/dbconfig-install boolean true
phpmyadmin      phpmyadmin/app-password-confirm password $DEFAULTPASS 
phpmyadmin      phpmyadmin/mysql/admin-pass     password $DEFAULTPASS
phpmyadmin      phpmyadmin/password-confirm     password $DEFAULTPASS
phpmyadmin      phpmyadmin/setup-password       password $DEFAULTPASS
phpmyadmin      phpmyadmin/mysql/app-pass       password $DEFAULTPASS
EOF

echo "--- Instalando pacotes basicos ---"
sudo apt-get install software-properties-common vim curl python-software-properties git-core --assume-yes --force-yes

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update
sudo apt-get -y upgrade

echo "--- Instalando MySQL, Phpmyadmin e alguns outros modulos ---"
sudo sudo apt-get install mysql-server --assume-yes --force-yes

echo "--- Instalando Phpmyadmin ---"
sudo apt-get install phpmyadmin --assume-yes --force-yes

echo "--- Instalando pacotes basicos ---"
sudo  apt-get install apt-transport-https lsb-release ca-certificates --assume-yes 

echo "--- Prover pacotes para Debian ---"
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg --assume-yes 

echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list --assume-yes 

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update
sudo apt-get -y upgrade

echo "--- Instalando PHP, Apache e alguns modulos ---"
sudo apt-get install php7.1 php7.1-common --assume-yes --force-yes
sudo apt-get install php7.1-cli libapache2-mod-php7.1 php7.1-xml php7.1-zip php7.1-mysql php7.1-gd php7.1-curl php-memcached php7.1-dev php7.1-mcrypt php7.1-sqlite3 php7.1-mbstring --assume-yes --force-yes

echo "--- Instalando PHP patchwork ---"
sudo apt-get install php-patchwork-utf8 --assume-yes --force-yes

echo "--- Escolhendo a vers�o padr�o ---"
sudo update-alternatives --set php /usr/bin/php7.1

echo "--- Habilitando unicamente o PHP 7.1 ---"
sudo a2dismod php5
sudo a2enmod php7.1

echo "--- Habilitando mod-rewrite do Apache ---"
sudo a2enmod rewrite

echo "--- Reiniciando Apache ---"
sudo service apache2 restart

echo "--- Baixando e Instalando Composer ---"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

echo "--- Baixando e Instalando Git---"
apt-get install git --assume-yes --force-yes
sudo apt-get -y install git --assume-yes --force-yes

echo "--- Instalando Banco NoSQL -> Redis <- ---" 
sudo apt-get install redis-server --assume-yes
sudo apt-get install php7.1-redis --assume-yes

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update
sudo apt-get -y upgrade

echo "--- Reiniciando Apache ---"
sudo service apache2 restart

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update
sudo apt-get -y upgrade

echo "[OK] --- Ambiente de desenvolvimento concluido ---"
